//
//  Triangle.swift
//  DelaunayTriangulationSwift
//
//  Created by Alex Littlejohn on 2016/01/08.
//  Copyright © 2016 zero. All rights reserved.
//

import CoreGraphics
import Foundation

/// A simple struct representing 3 vertices
@objc public class Triangle: NSObject {
    
    public init(vertex1: Vertex, vertex2: Vertex, vertex3: Vertex) {
        self.vertex1 = vertex1
        self.vertex2 = vertex2
        self.vertex3 = vertex3
        self.edge1 = Edge(vertex1: vertex1, vertex2: vertex2)
        self.edge2 = Edge(vertex1: vertex1, vertex2: vertex3)
        self.edge3 = Edge(vertex1: vertex2, vertex2: vertex3)
    }
    
    public let vertex1: Vertex
    public let vertex2: Vertex
    public let vertex3: Vertex
    public var normal: Vertex?
    
    public weak var neighbourOpposite1: Triangle?
    public weak var neighbourOpposite2: Triangle?
    public weak var neighbourOpposite3: Triangle?
  
    public let edge1: Edge
    public let edge2: Edge
    public let edge3: Edge
    
    private enum VertexTreandge {
        case vertexA
        case vertexB
        case vertexC
        case none
    }
    
    public func v1() -> CGPoint {
        return vertex1.pointValue()
    }
    
    public func v2() -> CGPoint {
        return vertex2.pointValue()
    }
    
    public func v3() -> CGPoint {
        return vertex3.pointValue()
    }
    
    public override var description : String {
        return """
                vertex1: \(self.vertex1), vertex2: \(vertex2) vertex3: \(vertex3)
                neighbourOpposite1 vertex1 \(String(describing: neighbourOpposite1?.vertex1))
                neighbourOpposite1 vertex2 \(String(describing: neighbourOpposite1?.vertex2))
                neighbourOpposite1 vertex3 \(String(describing: neighbourOpposite1?.vertex3))
                neighbourOpposite2 vertex1 \(String(describing: neighbourOpposite2?.vertex1))
                neighbourOpposite2 vertex2 \(String(describing: neighbourOpposite2?.vertex2))
                neighbourOpposite2 vertex3 \(String(describing: neighbourOpposite2?.vertex3))
                neighbourOpposite3 vertex1 \(String(describing: neighbourOpposite3?.vertex1))
                neighbourOpposite3 vertex2 \(String(describing: neighbourOpposite3?.vertex2))
                neighbourOpposite3 vertex3 \(String(describing: neighbourOpposite3?.vertex3))
               """;
    }
    
    public func setNeighbour(neighbour: Triangle) {
        var this: (VertexTreandge, VertexTreandge)
        var neighbourTreandge: (VertexTreandge, VertexTreandge)
        
        this = (.none, .none)
        neighbourTreandge = (.none, .none)
        
        var result = compareVertex(neighbour.vertex1)
        if result != .none {
            if (this.0 == .none) {this.0 = result} else {this.1 = result}
            if (neighbourTreandge.0 == .none) {neighbourTreandge.0 = result} else {neighbourTreandge.1 = result}
        }
        result = .none
        result = compareVertex(neighbour.vertex2)
        if result != .none {
            if (this.0 == .none) {this.0 = result} else {this.1 = result}
            if (neighbourTreandge.0 == .none) {neighbourTreandge.0 = result} else {neighbourTreandge.1 = result}
        }
        result = .none
        result = compareVertex(neighbour.vertex3)
        if result != .none {
            if (this.0 == .none) {this.0 = result} else {this.1 = result}
            if (neighbourTreandge.0 == .none) {neighbourTreandge.0 = result} else {neighbourTreandge.1 = result}
        }
        
        this = normalizeVertex(this)
        neighbourTreandge = normalizeVertex(neighbourTreandge)
        
        switch this {
        case (.vertexA, .vertexB) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexB:
            neighbourOpposite1 = neighbour
            neighbour.neighbourOpposite1 = self
        case (.vertexA, .vertexC) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexB:
            neighbourOpposite2 = neighbour
            neighbour.neighbourOpposite1 = self
        case (.vertexB, .vertexC) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexB:
            neighbourOpposite3 = neighbour
            neighbour.neighbourOpposite1 = self
            
        case (.vertexA, .vertexB) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexC:
            neighbourOpposite1 = neighbour
            neighbour.neighbourOpposite2 = self
        case (.vertexA, .vertexC) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexC:
            neighbourOpposite2 = neighbour
            neighbour.neighbourOpposite2 = self
        case (.vertexB, .vertexC) where neighbourTreandge.0 == .vertexA && neighbourTreandge.1 == .vertexC:
            neighbourOpposite3 = neighbour
            neighbour.neighbourOpposite2 = self
            
        case (.vertexA, .vertexB) where neighbourTreandge.0 == .vertexB && neighbourTreandge.1 == .vertexC:
            neighbourOpposite1 = neighbour
            neighbour.neighbourOpposite3 = self
        case (.vertexA, .vertexC) where neighbourTreandge.0 == .vertexB && neighbourTreandge.1 == .vertexC:
            neighbourOpposite2 = neighbour
            neighbour.neighbourOpposite3 = self
        case (.vertexB, .vertexC) where neighbourTreandge.0 == .vertexB && neighbourTreandge.1 == .vertexC:
            neighbourOpposite3 = neighbour
            neighbour.neighbourOpposite3 = self
            
        default:
            break
        }
        

//        switch neighbour {
//        case _ where edge1 == neighbour.edge1:
//            neighbourOpposite1 = neighbour
//            neighbour.neighbourOpposite1 = self
//        case _ where edge2 == neighbour.edge1:
//            neighbourOpposite2 = neighbour
//            neighbour.neighbourOpposite1 = self
//        case _ where edge3 == neighbour.edge1:
//            neighbourOpposite3 = neighbour
//            neighbour.neighbourOpposite1 = self
//
//        case _ where edge1 == neighbour.edge2:
//            neighbourOpposite1 = neighbour
//            neighbour.neighbourOpposite2 = self
//        case _ where edge2 == neighbour.edge2:
//            neighbourOpposite2 = neighbour
//            neighbour.neighbourOpposite2 = self
//        case _ where edge3 == neighbour.edge2:
//            neighbourOpposite3 = neighbour
//            neighbour.neighbourOpposite2 = self
//
//        case _ where edge1 == neighbour.edge3:
//            neighbourOpposite1 = neighbour
//            neighbour.neighbourOpposite3 = self
//        case _ where edge2 == neighbour.edge3:
//            neighbourOpposite2 = neighbour
//            neighbour.neighbourOpposite3 = self
//        case _ where edge3 == neighbour.edge3:
//            neighbourOpposite3 = neighbour
//            neighbour.neighbourOpposite3 = self
//
//        default:
//            break
//        }
    }
    
    public func isFindAllNeighbour() -> Bool {
        if neighbourOpposite1 != nil && neighbourOpposite2 != nil && neighbourOpposite3 != nil {
            return true
        }
        return false
    }
    
    private func compareVertex(_ compareVertex: Vertex) -> VertexTreandge {
       if vertex1 == compareVertex {
            return .vertexA
        }
        if vertex2 == compareVertex {
            return .vertexB
        }
        if vertex3 == compareVertex {
            return .vertexC
        }
        return .none
    }
    
     private func normalizeVertex(_ edge: (VertexTreandge, VertexTreandge)) -> (VertexTreandge, VertexTreandge) {
        var result = edge
        if (edge == (.vertexB, .vertexA)) {
            result = (.vertexA, .vertexB)
        }
        if (edge == (.vertexC, .vertexA)) {
            result = (.vertexA, .vertexC)
        }
        if (edge == (.vertexC, .vertexB)) {
            result = (.vertexB, .vertexC)
        }
        return result
    }
    

}
