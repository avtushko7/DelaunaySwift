//
//  Vertex.swift
//  DelaunayTriangulationSwift
//
//  Created by Alex Littlejohn on 2016/01/08.
//  Copyright © 2016 zero. All rights reserved.
//

import CoreGraphics
import Foundation

@objc public class Vertex: NSObject {
    
    public init(x: Double, y: Double) {
        self.x = x
        self.y = y
        self.z = 0
    }
    
    public init(x: Double, y: Double, z:Double) {
        self.x = x
        self.y = y
        self.z = z
    }
    
    public let x: Double
    public let y: Double
    public let z: Double
    
    public func pointValue() -> CGPoint {
        return CGPoint(x: x, y: y)
    }
    
//   public static func == (lhs: Vertex, rhs: Vertex) -> Bool {
//        return lhs.x == rhs.x && lhs.y == rhs.y
//    }
//    
//    public static func != (lhs: Vertex, rhs: Vertex) -> Bool {
//        return lhs.x != rhs.x && lhs.y != rhs.y
//    }
    
    public override var hashValue: Int {
        return "\(x)\(y)".hashValue
    }
    
    public override var description : String {
        return "x: \(self.x), y: \(y) z: \(z)";
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

extension Array where Element: Hashable {
    func removeDuplicates() -> [Element] {
        return Array(Set(self))
    }
}
