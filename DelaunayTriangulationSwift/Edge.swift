//
//  Edge.swift
//  DelaunaySwift
//
//  Created by Alex Littlejohn on 2016/04/07.
//  Copyright © 2016 zero. All rights reserved.
//


@objc public class Edge: NSObject {
    public let vertex1: Vertex
    public let vertex2: Vertex
    
    public init(vertex1: Vertex, vertex2: Vertex) {
        self.vertex1 = vertex1
        self.vertex2 = vertex2
    }
    
    static func == (lhs: Edge, rhs: Edge) -> Bool {
        return lhs.vertex1 == rhs.vertex1 && lhs.vertex2 == rhs.vertex2 || lhs.vertex1 == rhs.vertex2 && lhs.vertex2 == rhs.vertex1
    }
    
    override public var hashValue: Int {
        return "\(vertex1.x)\(vertex1.y)\(vertex2.x)\(vertex2.y)".hashValue
    }
    
    public override var description : String {
        return """
                Edge::  vertex1: \(self.vertex1), vertex2: \(vertex2)
               """;
    }
}

