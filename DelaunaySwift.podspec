Pod::Spec.new do |spec|
  spec.name               = "DelaunaySwift"
  spec.version            = "0.1.2"
  spec.summary            = "A utility for doing Delaunay triangulations on a set of vertices."
  spec.source             = { :git => "https://gitlab.com/avtushko7/DelaunaySwift.git", :tag => spec.version.to_s }
  spec.requires_arc       = true
  spec.platform           = :ios, "8.0"
  spec.license            = "MIT"
  spec.source_files       = "DelaunayTriangulationSwift/**/*.{swift}"
  spec.homepage           = "https://github.com/AlexLittlejohn/DelaunaySwift"
  spec.author             = { "Alex Littlejohn" => "alexlittlejohn@me.com" }
end
